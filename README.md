### Lab 2 - Chat Server

### Building and Testing

To build this project, run in the project root:

```
cabal sandbox init
cabal install --only-dependencies --enable-tests
cabal build
```

You can then test the chat server using telnet or netcat (nc) by running:

```
cabal run
```
and then running `telnet localhost <port>` in another terminal window. You can run the integration tests for this project with `cabal test`.

### Notes

The server works when it's run via `telnet` or `nc`, but I haven't been able to get the tests to work with `cabal test`. This has something to do with the way that the Network library acquires and releases sockets. Calling `chat` once from within a Haskell program seems to lock the port permanently for the remaining duration of the program, even when the thread in which chat is running is killed and the socket is closed via `sClose`. I'm not sure why this is or how to deal with it.  
