{-# LANGUAGE NamedFieldPuns #-}

-- | CS240h Lab 2 Chat Server
module Chat (chat, port) where

import Control.Applicative ((<$>), (<*>))
import Control.Concurrent (Chan, forkIO, writeChan, dupChan, newChan, readChan, forkFinally)
import Control.Exception (SomeException, catch)
import Control.Monad (forever)
import Data.Maybe (fromMaybe)
import Network (PortID(PortNumber), Socket, sClose, listenOn, accept, withSocketsDo)
import System.Environment (lookupEnv)
import System.Exit (exitSuccess)
import System.IO (Handle, hPutStrLn, hGetLine, hSetBuffering, BufferMode(NoBuffering))

-- | Listen on this port if the CHAT_SERVER_PORT environment variable is not set.
defaultPort :: Integer
defaultPort = 8001 

type UserId = Int

data ServerState = ServerState 
    { chatHistory :: Chan Message
    , nextId :: Int
    , socket :: Socket }

data UserState = UserState 
    { userChatHistory :: Chan Message
    , connection :: Handle
    , selfId :: UserId }

data MessageContents 
    = Containing String 
    | Quit
    | Joined

data Message = Message
    { contents :: MessageContents
    , sender :: UserId }

port :: IO PortID 
port = (PortNumber . fromInteger . fromMaybe defaultPort . fmap read) <$> 
    lookupEnv "CHAT_SERVER_PORT"

-- | Chat server entry point.
chat :: IO ()
chat = withSocketsDo $ catch runChatServer onException where
    socket = listenOn =<< port
    runChatServer = acceptConnections =<< (ServerState <$> newChan <*> return 1 <*> socket) 
   
    onException :: SomeException -> IO () 
    onException = \_ -> socket >>= sClose >> exitSuccess

-- | Called each time a new client connects to the chat server.
--   Spawns two new threads for servicing the client.
acceptConnections :: ServerState -> IO ()
acceptConnections serverState@(ServerState { chatHistory, nextId, socket }) = do 
    (handle, _, _) <- accept socket
    hSetBuffering handle NoBuffering
 
    userState <- UserState 
        <$> dupChan chatHistory 
        <*> return handle 
        <*> return nextId

    _ <- forkFinally (userInput userState) $ \_ -> 
            writeChan chatHistory $ Message Quit nextId
    _ <- forkIO $ userOutput userState

    writeChan chatHistory $ Message Joined nextId
 
    acceptConnections $ serverState { nextId = nextId + 1 }

-- | Watch for input from the client and write it to the broadcast channel.
userInput :: UserState -> IO ()
userInput (UserState { userChatHistory, selfId, connection }) = forever $ do
    inputLine <- hGetLine connection 
    writeChan userChatHistory $ Message 
        { contents = Containing inputLine
        , sender = selfId }

-- | Watch for messages on the broadcast channel. When there's a new message,
--   write it to the client's handle.
userOutput :: UserState -> IO ()
userOutput (UserState { userChatHistory, selfId, connection }) = forever $ do
    Message { contents, sender } <- readChan userChatHistory

    case contents of
        Quit           -> hPutStrLn connection $ show sender ++ " has left" 
        Joined         -> hPutStrLn connection $ show sender ++ " has joined" 
        Containing str -> 
            if sender == selfId 
            then return ()
            else hPutStrLn connection $ 
                show sender ++ ": " ++ str

