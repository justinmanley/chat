-- | Test our chat server.
module Main where

import Control.Concurrent (forkIO, killThread)
import Control.Exception (bracket)
import Control.Monad (replicateM)
import Network (connectTo)
import System.IO (Handle, hGetLine, hClose, hSetBuffering, hPutStrLn, BufferMode(NoBuffering))

import Test.Hspec
import Test.QuickCheck

import Chat

mkClient :: IO Handle 
mkClient = do 
    handle <- connectTo "localhost" =<< port
    hSetBuffering handle NoBuffering 

    return handle

withChat :: IO () -> IO ()
withChat test = bracket startChatServer stopChatServer (const test) where
    startChatServer = forkIO chat
    stopChatServer threadId = killThread threadId
    
main :: IO ()
main = do
    hspec $ describe "Testing Lab 2" $ do 

        describe "message" $ around_ withChat $ do
            it "own message not echoed" $ 
                let message = "my message"
                    lastLine = do
                        client <- mkClient

                        hPutStrLn client message
                        hGetLine client 
                in lastLine `shouldReturn` message 

        describe "client ids" $ around_ withChat $ do
            it "should increment with each new client" $ property $ \(Positive n) ->
                let nthClientHasJoined = do
                        clients <- replicateM n mkClient
                        
                        msg <- case reverse clients of
                            client:_ -> hGetLine client
                            []  -> fail "There should be at least one client."

                        mapM_ hClose clients

                        return msg
                in nthClientHasJoined `shouldReturn` (show n ++ " has joined") 

